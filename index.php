<?php

use App\Init;
use App\Core\Router;

error_reporting(E_ALL);
ini_set('display_errors', 1);
define('ROOT', __DIR__);

require_once ROOT . '/vendor/autoload.php';

Init::migrate('films');
Init::alter('films');
Init::alter('films_collate');
new Router();