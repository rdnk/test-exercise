<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test exercise</title>
    <link rel="stylesheet" href="/public/style.css">
</head>

<body class="container">
<main class="center">
    <header>
        <button class="import">
            <form action="/films/import/" method="post" enctype="multipart/form-data">
                <input type="file" name="import__file" id="import-file">
                <label for="import-file" class="import__file-button">
                    <span class="import__file-button-text">Импортировать файл</span>
                    <span class="icon-wrapper">
                        <img class="import__file-icon" src="/public/import.svg" alt="Выбрать файл" width="25">
                    </span>
                </label>
                <input class="hidden" type="submit" id="submit-file">
            </form>
        </button>
        <button class="search search-name">
            <form action="#" method="post" id="search-title">
                <input class="hidden" type="submit" id="submit-search-name">
                <input type="text" placeholder="Поиск по названию" name="search_name" id="search-name">
                <div class="search__results">
                </div>
                <span class="icon-wrapper icon-wrapper--search-title">
                    <img class="import__file-icon" src="/public/search.svg" alt="Поиск" width="25">
                </span>
            </form>
        </button>
        <button class="search search-actor">
            <form action="#" method="post" id="search-actor">
                <input class="hidden" type="submit" id="submit-search-actor">
                <input type="text" placeholder="Поиск по актёру" name="search_actor">
                <div class="search__results">
                </div>
                <span class="icon-wrapper icon-wrapper--search">
                    <img class="import__file-icon" src="/public/search.svg" alt="Поиск" width="25">
                </span>
            </form>
        </button>
    </header>
    <button class="btn btn-back"><a href="/">На главную</a></button>
    <div class="film">
        <?foreach ($data as $key => $value) :?>

            <div><?=$key.':'.'</div><div>'. $value?></div>

        <?endforeach; ?>
    </div>
</main>

<script src="/public/js/main.js"></script>
</body>

</html>