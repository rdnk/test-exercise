<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Test exercise</title>
    <link rel="stylesheet" href="/public/style.css">
</head>

<body class="container">
<aside>
    <a href="https://webbylab.com/" target="_blank">
        <img alt="company-logo" class="logo__img" src="data:image/svg+xml;base64,PD94bWwgdmVyc2lvbj0iMS4wIiBlbmNvZGluZz0idXRmLTgiPz4NCjwhLS0gR2VuZXJhdG9yOiBBZG9iZSBJbGx1c3RyYXRvciAyMy4wLjEsIFNWRyBFeHBvcnQgUGx1Zy1JbiAuIFNWRyBWZXJzaW9uOiA2LjAwIEJ1aWxkIDApICAtLT4NCjxzdmcgdmVyc2lvbj0iMS4xIiBpZD0iTGF5ZXJfMSIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIiB4bWxuczp4bGluaz0iaHR0cDovL3d3dy53My5vcmcvMTk5OS94bGluayIgeD0iMHB4IiB5PSIwcHgiDQoJIHZpZXdCb3g9IjAgMCA4MTkyIDI4MzQuNyIgc3R5bGU9ImVuYWJsZS1iYWNrZ3JvdW5kOm5ldyAwIDAgODE5MiAyODM0Ljc7IiB4bWw6c3BhY2U9InByZXNlcnZlIj4NCjxzdHlsZSB0eXBlPSJ0ZXh0L2NzcyI+DQoJLnN0MHtmaWxsOiMwRTUxN0Y7fQ0KCS5zdDF7ZmlsbDojOEFDMEUzO30NCgkuc3Qye2ZpbGw6IzFDODhDQTt9DQo8L3N0eWxlPg0KPGc+DQoJPGcgaWQ9IkxvZ29fM18iPg0KCQk8cG9seWdvbiBjbGFzcz0ic3QwIiBwb2ludHM9IjE5ODQuNiw3MjMuOSAxNTMxLDExNzcuNCAxNzI1LjYsMTM3Mi4xIDE5ODQuNiwxMTEzLjEgMjQ3Ni40LDE2MDQuOSAyNjcxLDE0MTAuMyAJCSIvPg0KCQk8Zz4NCgkJCTxwb2x5Z29uIGNsYXNzPSJzdDEiIHBvaW50cz0iMTQ1NC4xLDExNzcuMSAxMDAwLjksNzIzLjkgODA2LjMsOTE4LjUgMTI1OS41LDEzNzEuNyAJCQkiLz4NCgkJCTxnPg0KCQkJCTxwb2x5Z29uIGNsYXNzPSJzdDAiIHBvaW50cz0iMjQ3Ni40LDE2MDQuOSAzMTYyLjgsOTE4LjUgMjk2OC4yLDcyMy45IDIyODEuOCwxNDEwLjMgCQkJCSIvPg0KCQkJCTxwb2x5Z29uIGNsYXNzPSJzdDIiIHBvaW50cz0iMjI0My4zLDE0NDguOCAxOTg0LjYsMTcwNy41IDE0OTIuNiwxMjE1LjYgMTI5OCwxNDEwLjIgMTk4NC42LDIwOTYuNyAyNDM3LjksMTY0My40IAkJCQkiLz4NCgkJCTwvZz4NCgkJPC9nPg0KCTwvZz4NCgk8Zz4NCgkJPGc+DQoJCQk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNMzQ5Mi44LDEwOTUuNmMxMi44LDAsMTYuMiw3LjcsMTguOCwxNy45bDk1LjQsNTE4LjNsMTAzLjItNTE5LjJjMC45LTkuNCw3LjctMTcuMSwxNy4xLTE3LjFoNjcuMw0KCQkJCWMxMC4zLDAsMTYuMiw3LjcsMTguOCwxNy4xbDEwMy4yLDUxOS4ybDk0LjYtNTE4LjNjMi41LTEwLjMsNy43LTE3LjksMTguOC0xNy45aDUyLjljMTIuOCwwLDE3LjksNi44LDE2LjIsMTkuNmwtMTA2LjYsNTc4DQoJCQkJYy0wLjksMTAuMy03LjcsMTcuOS0xNy45LDE3LjloLTEwMi4zYy0xMC4zLDAtMTYuMi02LTE3LjEtMTcuMWwtOTMuOS00NzUuN2wtOTMuOCw0NzUuN2MtMS43LDExLjEtNy43LDE3LjEtMTcuOSwxNy4xaC0xMDEuNA0KCQkJCWMtMTAuMywwLTE2LjItNy43LTE4LjgtMTcuOWwtMTA2LjYtNTc4LjhjLTEuNy0xMi44LDMuNC0xOC44LDE2LjItMTguOEwzNDkyLjgsMTA5NS42TDM0OTIuOCwxMDk1LjZ6Ii8+DQoJCQk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNDE3Mi4yLDEzNzUuMmMwLTU3LjksMzguNC05NC42LDkzLjgtOTQuNmgxNjcuMWM1NS40LDAsOTMuOCwzNi42LDkzLjgsOTQuNnY3Mi41YzAsNTQuNS0zOS4yLDkyLTkxLjMsOTINCgkJCQlINDI2MHY0NmMwLDMxLjYsMjEuMyw1Miw1Miw1MmgxNjUuNGMxMi44LDAsMTkuNiw2LDE5LjYsMTguOHYzNWMwLDEyLjgtNi44LDE5LjYtMTkuNiwxOS42aC0yMDguOWMtNTcuOSwwLTk2LjMtMzguNC05Ni4zLTk3LjINCgkJCQlWMTM3NS4yeiBNNDI2MCwxNDk0LjVsMTQ0LjEtMTEuOWMyNi40LTIuNSw0MC45LTE5LjYsNDAuOS00My41VjEzOTRjMC0yNi40LTE3LjktNDQuNC00Mi42LTQ0LjRoLTk3LjINCgkJCQljLTI2LjQsMC00NS4yLDE3LjEtNDUuMiw0NS4yVjE0OTQuNXoiLz4NCgkJCTxwYXRoIGNsYXNzPSJzdDAiIGQ9Ik00NzAyLjQsMTAzNC4yYzEyLjgsMCwxOS42LDYuOCwxOS42LDE5LjZ2MjQzbDE0Mi4zLTEzLjdjMTYuMi0yLjUsMzMuMi0yLjUsMzkuMi0yLjUNCgkJCQljNTcuMSwwLDkyLjksMzYuNiw5Mi45LDkzLjh2MjQ1LjVjMCw1NS40LTM3LjUsOTMuOC05Mi45LDkzLjhjLTYuOCwwLTI3LjMtMi41LTQ0LjQtMy40bC0xMzcuMy0xMy43bC0xMS4xLDE0LjVoLTU3LjENCgkJCQljLTEyLjgsMC0xOS42LTYuOC0xOS42LTIwLjR2LTYzNi45YzAtMTIuOCw2LjgtMTkuNiwxOS42LTE5LjZINDcwMi40eiBNNDcyMiwxMzUzLjl2Mjg2LjRoMTQwLjdjMjguMSwwLDQ2LTE3LjksNDYtNDZ2LTE5NC4zDQoJCQkJYzAtMjcuMy0xNy45LTQ2LTQ2LTQ2TDQ3MjIsMTM1My45TDQ3MjIsMTM1My45eiIvPg0KCQkJPHBhdGggY2xhc3M9InN0MCIgZD0iTTUxODAuNiwxMDM0LjJjMTIuOCwwLDE5LjYsNi44LDE5LjYsMTkuNnYyNDNsMTQyLjMtMTMuN2MxNi4yLTIuNSwzMy4yLTIuNSwzOS4yLTIuNQ0KCQkJCWM1Ny4xLDAsOTIuOSwzNi42LDkyLjksOTMuOHYyNDUuNWMwLDU1LjQtMzcuNSw5My44LTkyLjksOTMuOGMtNi44LDAtMjcuMy0yLjUtNDQuNC0zLjRsLTEzNy4zLTEzLjdsLTExLjEsMTQuNWgtNTcuMQ0KCQkJCWMtMTIuOCwwLTE5LjYtNi44LTE5LjYtMjAuNHYtNjM2LjljMC0xMi44LDYuOC0xOS42LDE5LjYtMTkuNkg1MTgwLjZ6IE01MjAwLjIsMTM1My45djI4Ni40aDE0MC43YzI4LjEsMCw0Ni0xNy45LDQ2LTQ2di0xOTQuMw0KCQkJCWMwLTI3LjMtMTcuOS00Ni00Ni00Nkw1MjAwLjIsMTM1My45TDUyMDAuMiwxMzUzLjl6Ii8+DQoJCQk8cGF0aCBjbGFzcz0ic3QwIiBkPSJNNTYzNSwxMjgwLjVjOS40LDAsMTQuNSw0LjMsMTcuMSwxMy43bDEwMS40LDM1Mi4xbDEwMi4zLTM1MS4yYzIuNS03LjcsMTAuMy0xNC41LDE3LjktMTQuNWg1Mg0KCQkJCWMxMS4xLDAsMjEuMyw4LjUsMTcuMSwyMS4zbC0xNjcuOSw1NzhjLTEuNywxMS4xLTcuNywxNi4yLTE3LjEsMTYuMmgtNTAuM2MtMTEuMSwwLTIxLjMtMTAuMy0xOC44LTIzbDQ4LjYtMTYyaC00MS44DQoJCQkJYy05LjQsMC0xNS4zLTUuMS0xNy4xLTE0LjVsLTExMy40LTM5My44Yy0yLjUtMTIuOCw0LjMtMjIuMiwxNy45LTIyLjJMNTYzNSwxMjgwLjVMNTYzNSwxMjgwLjV6Ii8+DQoJCTwvZz4NCgkJPGc+DQoJCQk8cGF0aCBjbGFzcz0ic3QyIiBkPSJNNjIyMS41LDE2MjUuOGMwLDYsNiwxMS45LDExLjksMTEuOWgyMzQuNGMxMi44LDAsMTkuNiw2LjgsMTkuNiwxOS42djM0LjFjMCwxMi44LTYuOCwxOS42LTE5LjYsMTkuNg0KCQkJCWgtMzE0LjZjLTEyLjgsMC0xOS42LTYuOC0xOS42LTE5LjZ2LTU3Ni4zYzAtMTIuOCw2LjgtMTkuNiwxOS42LTE5LjZoNDguNmMxMi44LDAsMTkuNiw2LjgsMTkuNiwxOS42TDYyMjEuNSwxNjI1LjgNCgkJCQlMNjIyMS41LDE2MjUuOHoiLz4NCgkJCTxwYXRoIGNsYXNzPSJzdDIiIGQ9Ik02ODE5LjEsMTI4MC41YzU3LjksMCw5Ny4yLDM5LjIsOTcuMiw5OHYzMTJjMCwxMy43LTYuOCwyMC40LTE5LjYsMjAuNGgtNTcuMWwtMTEuMS0xNC41bC0xMzAuNCwxMy43DQoJCQkJYy0xOS42LDIuNS0zNy41LDMuNC00My41LDMuNGMtNTUuNCwwLTkyLjktMzguNC05Mi45LTkzLjhWMTU0M2MwLTU1LjQsMzcuNS05My44LDkyLjktOTMuOGM2LDAsMjMsMC45LDM5LjIsMy40bDEzNC43LDEzLjcNCgkJCQl2LTYyLjJjMC0zMy4yLTIwLjQtNTAuMy01My43LTUwLjNoLTE3Mi4yYy0xMi44LDAtMTkuNi02LjgtMTkuNi0xOS42di0zNC4xYzAtMTIuOCw2LjgtMTkuNiwxOS42LTE5LjZMNjgxOS4xLDEyODAuNQ0KCQkJCUw2ODE5LjEsMTI4MC41eiBNNjY4Ny44LDE1MTMuM2MtMjYuNCwwLTQ0LjQsMTcuMS00NC40LDQ0LjR2NDQuNGMwLDI3LjMsMTcuOSw0NS4yLDQ0LjQsNDUuMmgxNDAuN3YtMTMzLjhMNjY4Ny44LDE1MTMuMw0KCQkJCUw2Njg3LjgsMTUxMy4zeiIvPg0KCQkJPHBhdGggY2xhc3M9InN0MiIgZD0iTTcxMDAuNCwxMDM0LjJjMTIuOCwwLDE5LjYsNi44LDE5LjYsMTkuNnYyNDNsMTQyLjMtMTMuN2MxNi4yLTIuNSwzMy4yLTIuNSwzOS4yLTIuNQ0KCQkJCWM1Ny4xLDAsOTIuOSwzNi42LDkyLjksOTMuOHYyNDUuNWMwLDU1LjQtMzcuNSw5My44LTkyLjksOTMuOGMtNi44LDAtMjcuMy0yLjUtNDQuNC0zLjRsLTEzNy4zLTEzLjdsLTExLjEsMTQuNWgtNTcuMQ0KCQkJCWMtMTIuOCwwLTE5LjYtNi44LTE5LjYtMjAuNHYtNjM2LjljMC0xMi44LDYuOC0xOS42LDE5LjYtMTkuNkg3MTAwLjR6IE03MTE5LjksMTM1My45djI4Ni40aDE0MC43YzI4LjEsMCw0Ni0xNy45LDQ2LTQ2di0xOTQuMw0KCQkJCWMwLTI3LjMtMTcuOS00Ni00Ni00Nkw3MTE5LjksMTM1My45TDcxMTkuOSwxMzUzLjl6Ii8+DQoJCTwvZz4NCgk8L2c+DQo8L2c+DQo8L3N2Zz4NCg==">
    </a>
    <form action="/films/add/" class="add-film" method="post">
        <h4>Добавить фильм</h4>
        <div class="input__wrapper">
            <label for="title">Название</label>
            <input type="text" name="title" id="title">
        </div>
        <div class="input__wrapper">
            <label for="year">Год выпуска</label>
            <input type="number" name="year" id="year">
        </div>
        <div class="input__wrapper">
            <label for="format">Формат</label>
<!--            <input type="text" name="format" id="format">-->
            <select name="format" id="format">
                <option value="DVD" selected>DVD</option>
                <option value="VHS">VHS</option>
                <option value="Blu-Ray">Blu-Ray</option>
            </select>
        </div>
        <div class="input__wrapper">
            <label for="actors">Список актёров</label>
            <input type="text" name="actors" id="actors">
        </div>
        <input class="btn btn-submit" type="submit" value="Добавить">
    </form>
    <div class="aside__footer">
        <a href="https://www.linkedin.com/in/konstantin-rudnik-ab11191a5/">LinkedIn</a>
    </div>
</aside>
<main>
    <div class="error">
    </div>
    <header>
        <button class="import">
            <form id="form-import" action="/films/import/" method="post" enctype="multipart/form-data">
                <input type="file" name="import__file" id="import-file">
                <label for="import-file" class="import__file-button">
                    <span class="import__file-button-text">Импортировать файл</span>
                    <span class="icon-wrapper">
                        <img class="import__file-icon" src="/public/import.svg" alt="Выбрать файл" width="25">
                    </span>
                </label>
                <input class="hidden" type="submit" id="submit-file">
            </form>
        </button>
        <button class="search search-name">
            <form action="#" method="post" id="search-title">
                <input class="hidden" type="submit" id="submit-search-name">
                <input type="text" placeholder="Поиск по названию" name="search_name" id="search-name">
                <div class="search__results">
                </div>
                <span class="icon-wrapper icon-wrapper--search-title">
                    <img class="import__file-icon" src="/public/search.svg" alt="Поиск" width="25">
                </span>
            </form>
        </button>
        <button class="search search-actor">
            <form action="#" method="post" id="search-actor">
                <input class="hidden" type="submit" id="submit-search-actor">
                <input type="text" placeholder="Поиск по актёру" name="search_actor">
                <div class="search__results">
                </div>
                <span class="icon-wrapper icon-wrapper--search">
                    <img class="import__file-icon" src="/public/search.svg" alt="Поиск" width="25">
                </span>
            </form>
        </button>
    </header>
    <section class="wrapper">
        <div class="wrapper__header format_table">
            <span class="wrapper__header_id">Id</span>
            <a href="/films/sort/"><span class="wrapper__header_title">Название</span></a>
            <span class="wrapper__header_year">Год выпуска</span>
            <span class="wrapper__header_format">Формат</span>
            <span class="wrapper__header_actors">Список актёров</span>
        </div>
        <div class="wrapper__body">
            <?php foreach($data['films'] as $film): ?>
            <div class="row format_table">
                <span class="row_id"><?=$film['id'] ?></span>
                <span class="row_title"><a href="/films/show/<?=$film['id'] ?>"><?=$film['title'] ?></a></span>
                <span class="row_year"><?=$film['year'] ?></span>
                <span class="row_format"><?=$film['format'] ?></span>
                <span class="row_actors"><?=$film['actors'] ?></span>
                <span class="row_delete-btn">
                    <a class="btn__link" href="/films/delete/<?=$film['id']?>"><div class="btn btn-delete">Удалить</div></a>
                </span>
            </div>
            <?php endforeach; ?>
        </div>
    </section>
</main>

<script src="/public/js/main.js"></script>
<script src="/public/js/formValidate.js"></script>
</body>

</html>