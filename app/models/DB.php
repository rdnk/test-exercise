<?php


namespace App\Models;


use PDO;

class DB
{
    private static $_db = null;

    public static function getInstance(): ?PDO
    {
        if (self::$_db === null) {
            self::$_db = new PDO('mysql:host='.DB_HOST.';dbname='.DB_NAME.'', ''.DB_USER.'', ''.DB_PASS.'');
        }

        return self::$_db;
    }

    private function __construct() {}
    private function __clone() {}
    private function __wakeup() {}
}