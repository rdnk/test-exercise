<?php


namespace App\Models;


use PDO;

class Films extends Model implements iFilms
{
    private string $table = 'films';

    public function getAllMovies($column = 'id', $sortType = 'ASC')
    {
        $query = $this->_db->query("SELECT * FROM $this->table ORDER BY `$column` $sortType");

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function getMovie($id)
    {
        $query = $this->_db->query("SELECT * FROM $this->table WHERE `id` = $id");

        $result = $query->fetch(PDO::FETCH_ASSOC);

        $rus = [
            'Идентификатор',
            'Название',
            'Год выпуска',
            'Формат',
            'Актёры'
        ];

        return array_combine($rus, $result);
    }

    public function addMovie($data)
    {
        if($this->isValidData($data)) {
            $this->_db->exec("INSERT INTO $this->table (`id`, `title`, `year`, `format`, `actors`) VALUES (NULL, '{$data['title']}', '{$data['year']}', '{$data['format']}', '{$data['actors']}')");
        }
    }

    public function removeMovie($id)
    {
        return $this->_db->query("DELETE FROM $this->table WHERE $this->table.`id` = $id");
    }

    public function findMovie(string $value, $column)
    {
        $query = $this->_db->query("SELECT `id`,`$column` FROM $this->table WHERE `$column` LIKE '%$value%' COLLATE utf8_general_ci");
        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    public function importMoviesFromTxt($file)
    {
        // TODO: Implement importMoviesFromTxt() method.
    }

    private function isValidData($data)
    {
        foreach ($data as $key => $item) {
            if (trim($item) === '') {
                return false;
            }

            if ($key === 'year') {
                if (!is_numeric($item) || $item <= 1850 || $item > date('Y')) {
                    return false;
                }
            }
        }

        return true;
    }
}