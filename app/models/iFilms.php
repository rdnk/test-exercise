<?php


namespace App\Models;


interface iFilms
{
    public function getAllMovies($column, $sortType);
    public function getMovie($id);
    public function addMovie($data);
    public function removeMovie($id);
    public function findMovie(string $value, $column);
    public function importMoviesFromTxt($file);
}