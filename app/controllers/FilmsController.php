<?php


namespace App\Controllers;


use App\Core\Controller;
use App\Core\UploadHelper;

class FilmsController extends Controller
{
    use UploadHelper;

    public function index(): void
    {
        $type = $_COOKIE['sortType'] ?? 'ASC';

        $model = $this->model('Films');
        $data['films'] = $model->getAllMovies('title', $type);

        $this->view('main/index', $data);
    }

    public function delete($id): void
    {
        $model = $this->model('Films');
        if (isset($id)) {
            $model->removeMovie($id);
        }

        header("Location: /");
    }

    public function add(): void
    {
        $model = $this->model('Films');
        if (isset($_POST['title'])) {
            $data = [
                'title' => $_POST['title'],
                'year' => $_POST['year'],
                'format' => $_POST['format'],
                'actors' => $_POST['actors'],
            ];


            $model->addMovie($data);
        }

        echo 'true';exit();
//        header("Location: /");
    }

    public function import()
    {
        $model = $this->model('Films');

        $valid = $this->can_upload($_FILES['import__file']);
        if ($valid === true) {
            if (isset($_FILES['import__file'])) {
                $result = $this->parseFile($_FILES['import__file']);
            }

            foreach ($result as $data) {
                $model->addMovie($data);
            }
        }


        header("Location: /");
    }

    public function search()
    {
        header('Content-type: application/json');
        $model = $this->model('Films');

        if (isset($_POST['search_name'])) {
            $res = $model->findMovie($_POST['search_name'], 'title');
        }

        if (isset($_POST['search_actor']) && !empty(trim($_POST['search_actor']))) {
            $res = $model->findMovie($_POST['search_actor'], 'actors');
            foreach ($res as &$el) {
                $temp = explode(', ', $el['actors']);
                $temp = array_filter($temp, static function ($actor) {
                    return stripos($actor, $_POST['search_actor']) !== false;
                });
                $el['actors'] = array_values($temp);
            }
            unset($el);

            $res = array_column($res, 'actors');
            $temp = [];
            foreach ($res as $array) {
                $temp[] = current($array);
            }
            $res = array_values(array_unique($temp));
            unset($temp);
        }

        if(empty($res)) {
            $res = ['Нет результатов'];
        }
        echo json_encode($res);exit();
    }

    public function show($id)
    {
        $model = $this->model('Films');
        $data = $model->getMovie($id);

        $this->view('main/show', $data);
    }

    public function sort() {
        if (!isset($_COOKIE['sortType'])) {
            $type = 'DESC';
            setcookie('sortType', $type, time() + 3600, '/');
        } else {
            if ($_COOKIE['sortType'] === 'DESC') {
                setcookie('sortType', 'ASC', time() + 3600, '/');
            }

            if ($_COOKIE['sortType'] === 'ASC') {
                @setcookie('sortType', 'DESC', time() + 3600, '/');
            }

            $type = $_COOKIE['sortType'];
        }

        $availableTypes = ['ASC', 'DESC'];
        if (!in_array(strtoupper($type), $availableTypes, true)) {
            exit();
        }
        header("Location: /");
//        $model = $this->model('Films');
//        $data['films'] = $model->getAllMovies('title', $type);
//        $this->view('main/index', $data);

    }
}