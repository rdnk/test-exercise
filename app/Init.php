<?php


namespace App;


use App\Models\DB;

class Init
{
    public static function migrate($table): void
    {
        $db = DB::getInstance();
        $migration = file_get_contents(ROOT."/app/migrations/$table");
        if ($db->query("SHOW TABLES LIKE '$table'")->fetch() === false) {
            $db->exec($migration);
        }
    }

    public static function alter($table): void
    {
        $db = DB::getInstance();
        $migration = file_get_contents(ROOT."/app/migrations/alter_$table");
        $db->exec($migration);
    }
}