<?php


namespace App\Core;


class Controller
{
    protected function model($model) {
        $class = 'App\Models\\'.$model;
        return new $class;
    }

    protected function view($view, $data = []) {
        require_once ROOT.'/app/views/'. $view . '.php';
    }
}