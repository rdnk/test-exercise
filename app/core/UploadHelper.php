<?php

namespace App\Core;

trait UploadHelper
{
    public function can_upload($file)
    {
        $result = true;
        if ($file['name'] === '') {
            $result = 'Вы не выбрали файл.';
        }

        if ($file['size'] === 0) {
            $result = 'Файл слишком большой.';
        }

        $getMime = explode('.', $file['name']);
        $mime = strtolower(end($getMime));
        $types = ['txt'];

        if (!in_array($mime, $types)) {
            $result = 'Недопустимый тип файла.';
        }

        return $result;
    }

    public function make_upload($file): void
    {
        $name = random_int(0, 10000) . $file['name'];
        copy($file['tmp_name'], 'img/' . $name);
    }

    private function parseFile($file)
    {
        $res = file_get_contents($file['tmp_name']);
        $res = array_filter(explode(PHP_EOL, $res));
        $res = array_chunk($res, 4);

        $result = [];
        foreach ($res as $record) {
            foreach ($record as $el) {
                $needle = ': ';
                $needleLength = strlen($needle);
                $temp = strpos($el, ': ');
                $key = lcfirst(substr($el, 0, $temp));
                $value = substr($el, $temp+$needleLength);

                if(strpos($key, 'Year') !== false) {
                    $key = 'year';
                }
                if(strpos($key, 'stars') !== false) {
                    $key = 'actors';
                }

                $data[$key] = $value;
            }
            $result[] = $data;
            unset($data);
        }
        unset($res);

        return $result;
    }
}