<?php


namespace App\Core;


class Router
{
    protected $controller = 'FilmsController';
    protected string $method = 'index';
    protected array $params = [];

    public function __construct()
    {
        $url = $this->parseUrl();

        if (isset($url[0]) && file_exists(ROOT.'/app/controllers/' . ucfirst($url[0]) . 'Controller.php')) {
            $this->controller = ucfirst($url[0].'Controller');
            unset($url[0]);
        }

        $class = 'App\Controllers\\'.$this->controller;
        $this->controller = new $class;
        if (isset($url[1]) && method_exists($this->controller, $url[1])) {
            $this->method = $url[1];
            unset($url[1]);
        }

        $this->params = $url ? array_values($url) : [];
        call_user_func_array([$this->controller, $this->method], $this->params);
    }

    /**
     * 
    */
    public function parseUrl(): array
    {
        $parsed = [];
        if (isset($_GET['url'])) {
            $parsed = explode('/', filter_var(
                rtrim($_GET['url'], '/'),
                FILTER_SANITIZE_STRING
            ));
        }

        return $parsed;
    }
}