const inputElement = document.getElementById("import-file");
const submitFile = document.getElementById('submit-file');
inputElement.addEventListener("change", handleFiles, false);
function handleFiles() {
    if (uploadFile(inputElement.files[0]) === true) {
        let reader = new FileReader();
        reader.onload = submitFile.click();
    }
}

function uploadFile(file) {
    if (file.type !== 'text/plain') {
        alert('Разрешены только текстовые файлы')
        inputElement.value  = '';
        return;
    }

    if (file.size > 2 * 1024 * 1024) {
        alert('Файл должен быть менее 2МБ.')
        return;
    }

    return true;
}


/* Search */
const searchTitleBtn = document.querySelector('.icon-wrapper--search-title')
const searchTitleSubmit = document.getElementById('submit-search-name')
const searchTitleForm = document.getElementById('search-title')
const searchTitleResults = searchTitleForm.querySelector('.search__results')
const searchActorBtn = document.querySelector('.icon-wrapper--search')
const searchActorSubmit = document.getElementById('submit-search-actor')
const searchActorForm = document.getElementById('search-actor')
const searchActorResults = searchActorForm.querySelector('.search__results')

searchTitleBtn.addEventListener('click', () => {
    searchTitleSubmit.click();
})

searchActorBtn.addEventListener('click', () => {
    searchActorSubmit.click();
})

searchTitleForm.addEventListener('submit', formSend);
searchActorForm.addEventListener('submit', formSend);

async function formSend(e) {
    e.preventDefault();

    let formData = new FormData(this);

    let response = await fetch('/films/search/', {
        method: 'POST',
        body: formData
    });
    if (response.ok) {
        let result = await response.json();
        console.log(result)
        let searchResults = this.querySelector('.search__results');

        searchResults.style.display = 'block';
        while (searchResults.firstChild) {
            searchResults.removeChild(searchResults.firstChild);
        }
        result.forEach(el => {
            if (this.getAttribute('id') === 'search-title') {
                if (el instanceof Object) {
                    addSearchResult(searchResults, el.title, el.id)
                } else {
                    addSearchResult(searchResults, el)
                }
            } else {
                if (el.actors instanceof Object) {
                    for (let actor of el.actors) {
                        addSearchResult(searchResults, actor, el.id)
                    }
                } else {
                    addSearchResult(searchResults, el)
                }
            }
        })
    }
}

function addSearchResult(searchResults, context, id = null) {
    let p = document.createElement('p');
    let a = document.createElement('a');
    if (id !== null) {
        a.setAttribute('href', '/films/show/'+id)
    }
    p.append(a)
    a.textContent = context;
    searchResults.appendChild(p)
}

document.addEventListener('click', () => {
    searchTitleResults.style.display = 'none';
    searchActorResults.style.display = 'none';
})

const delButtons = document.querySelectorAll('.btn__link')

delButtons.forEach(el => {
    el.addEventListener('click', e => {
        let acceptDeletion = confirm('Вы уверены?')
        if (!acceptDeletion) {
            e.preventDefault()
        }
    })
})