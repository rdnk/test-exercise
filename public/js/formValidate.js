const form = document.querySelector('.add-film')
const errorBlock = document.querySelector('.error')

form.addEventListener('submit', e => {
    e.preventDefault();

    let errors = [];
    let formData = new FormData(form);

    for (let item of formData.entries()) {
        let itemName = form.querySelector('[for=' + item[0] + ']').textContent
        if (item[1].trim() === '') {
            errors.push(itemName+' необходимо заполнить')
        }

        if (item[0] === 'year') {
            if (+item[1] < 1850 || +item[1] > 2021) {
                errors.push(itemName+' неправильно указан')
            }
        }

        if (item[0] === 'actors') {
            let actorsArr = item[1].split(',')
            actorsArr = actorsArr.map(el => el.trim() )

            let sameActors = false;
            let temp = arrayCountValues(actorsArr)

            for (let i in temp) {
                if (temp[i] > 1) {
                    sameActors = true;
                }
            }

            if (sameActors === true) {
                errors.push('Некоторые актёры повторяются')
            }
        }
    }

    if (errors.length > 0) {
        echoErrors(errors)
    } else {
        fetch('/films/add/', {
            method: 'POST',
            body: formData
        });
        
        alert('Фильм добавлен')
        window.location.href = '/'
    }
})

function echoErrors(errors) {
    while (errorBlock.firstChild) {
        errorBlock.removeChild(errorBlock.firstChild);
    }

    for (let err of errors) {
        let p = document.createElement('P')
        p.textContent = err;
        errorBlock.prepend(p)
    }
}

function arrayCountValues(arr) {
    let v, freqs = {};

    for (let i = arr.length; i--; ) {
        v = arr[i];
        if (freqs[v]) freqs[v] += 1;
        else freqs[v] = 1;
    }
    return freqs;
}